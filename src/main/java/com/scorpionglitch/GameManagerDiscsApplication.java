package com.scorpionglitch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameManagerDiscsApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameManagerDiscsApplication.class, args);
	}

}
